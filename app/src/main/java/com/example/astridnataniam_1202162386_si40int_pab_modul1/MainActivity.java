package com.example.astridnataniam_1202162386_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editAlas;
    private EditText editTinggi;
    private TextView editHasil;
    public Button buttoncek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editAlas = findViewById(R.id.textEdit1);
        editTinggi =  findViewById(R.id.textEdit2);
        editHasil = findViewById(R.id.textView1);
        buttoncek = findViewById(R.id.butonn);

        Integer alas = Integer.parseInt(editAlas.getText().toString());
        Integer tinggi = Integer.parseInt(editTinggi.getText().toString());
        Integer hasil = alas*tinggi;
        editHasil.setText(String.valueOf(hasil));
}
